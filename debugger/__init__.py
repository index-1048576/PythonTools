import time
def mytimeit(function):
    def wrapper(*a,**kw):
        a = time.time()
        function(*a,**kw)
        b = time.time()
        print(f"{b-a}second left")
    return wrapper

def debugger(function):
    pass